import { Component } from "react";
import "./createproduct.css";

class CreateProduct extends Component {
  state = {
    name: "",
    description: "",
    price: "",
    discount: "",
    merchantname: "",
    quantity: "",
    err_msg: "",
    is_data: false,
  };

  changeName = (event) => {
    this.setState({ name: event.target.value });
  };

  changeDescription = (event) => {
    this.setState({ description: event.target.value });
  };

  changePrice = (event) => {
    this.setState({ price: event.target.value });
  };

  changeDiscount = (event) => {
    this.setState({ discount: event.target.value });
  };

  merchantName = (event) => {
    this.setState({ merchantname: event.target.value });
  };

  changeQuantity = (event) => {
    this.setState({ quantity: event.target.value });
  };

  submitingUserData = async (event) => {
    event.preventDefault();
    const url = "http://localhost:1947/product/create";
    const { name, description, price, quantity, merchantname, discount } =
      this.state;
    const bodyData = {
      name,
      description,
      price,
      quantity,
      merchant_name: merchantname,
      discount,
    };
    const option = {
      method: "POST",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      this.setState({
        name: "",
        description: "",
        merchantname: "",
        discount: "",
        price: "",
        quantity: "",
        err_msg: "",
        is_data: false,
      });
      alert("data added successfully");
    } else {
      this.setState({ err_msg: data.status_message, is_data: true });
    }
  };

  render() {
    const { is_data, err_msg } = this.state;
    return (
      <div className="form animated flipInX">
        <h2>Create products</h2>
        <form onSubmit={this.submitingUserData}>
          <input
            placeholder="product name"
            type="text"
            onChange={this.changeName}
          />
          <input
            placeholder="description"
            type="text"
            onChange={this.changeDescription}
          />
          <input placeholder="price" type="text" onChange={this.changePrice} />
          <input
            placeholder="discount"
            type="text"
            onChange={this.changeDiscount}
          />
          <input
            placeholder="merchant name"
            type="text"
            onChange={this.merchantName}
          />
          <input
            placeholder="quantity"
            type="text"
            onChange={this.changeQuantity}
          />
          <button type="submit">Create</button>
          {is_data && <p className="para-1">* {err_msg}</p>}
        </form>
      </div>
    );
  }
}

export default CreateProduct;
