import "./sidebar.css";

const Sidebar = () => {
  return (
    <div>
      {/* The sidebar */}
      <div className="sidebar">
        <a className="active" href="/">
          Products
        </a>
        <a href="/create_product">create product</a>
        <a href="/update_product">update product</a>
        <a href="/delete_product">delete product</a>
      </div>
      {/* Page content */}
      <div className="content">..</div>
    </div>
  );
};
export default Sidebar;
