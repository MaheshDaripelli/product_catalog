import { Component } from "react";
import "./deleteproduct.css";

class DeleteProduct extends Component {
  state = {
    id: "",
  };

  changeid = (event) => {
    this.setState({ id: event.target.value });
  };

  submitingUserData = async (event) => {
    event.preventDefault();
    const url = "http://localhost:1947/product/delete";
    const { id } = this.state;
    const bodyData = {
      id,
    };
    const option = {
      method: "DELETE",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      this.setState({
        id: "",
      });
      alert("data deleted successfully");
    }
  };

  render() {
    return (
      <div className="form animated flipInX">
        <h2>Create products</h2>
        <form onSubmit={this.submitingUserData}>
          <input placeholder="id" type="text" onChange={this.changeid} />
          <button type="submit">Delete</button>
        </form>
      </div>
    );
  }
}

export default DeleteProduct;
