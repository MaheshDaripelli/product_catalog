import "./header.css";

const Header = () => {
  return (
    <div className="header">
      <h2>Product Catalog</h2>
      <div className="profile">
        <label>D.Mahesh</label>
      </div>
    </div>
  );
};

export default Header;
