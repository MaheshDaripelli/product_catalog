import { Component } from "react";
import Product from "../Product/product";
import "./productview.css";

class ProductView extends Component {
  state = { productData: [] };
  componentDidMount() {
    this.productApiCall();
  }
  productApiCall = async () => {
    const url = "http://localhost:1947/products";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    this.setState({ productData: data });
  };
  render() {
    const { productData } = this.state;
    return (
      <>
        <div className="og-contianer">
          <h1 className="heading-line">Products</h1>
          <div className="og-row og-li og-li-head">
            <div className="og-li-col og-li-col-1 text-center">#</div>
            <div className="og-li-col og-li-col-2">Name</div>
            <div className="og-li-col og-li-col-3 text-center">description</div>
            <div className="og-li-col og-li-col-4 text-center">price</div>
            <div className="og-li-col og-li-col-5 text-center">discount</div>
            <div className="og-li-col og-li-col-6 text-center">
              Merchant name
            </div>
            <div className="og-li-col og-li-col-7 text-center">Quantity</div>
          </div>
          {productData.map((each) => (
            <Product each={each} key={each.id} />
          ))}
        </div>
      </>
    );
  }
}
export default ProductView;
