import { Component } from "react";
import "./updateproduct.css";

class UpdateProduct extends Component {
  state = {
    id: "",
    price: "",
  };

  changeId = (event) => {
    this.setState({ id: event.target.value });
  };

  changePrice = (event) => {
    this.setState({ price: event.target.value });
  };

  submitingUpdatedData = async (event) => {
    console.log("hello");
    event.preventDefault();
    console.log("hello");
    const url = "http://localhost:1947/update";
    const { id, price } = this.state;
    const bodyData = {
      id,
      price,
    };
    const option = {
      method: "PUT",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      this.setState({
        id: "",
        price: "",
      });
      alert("data added successfully");
    }
  };

  render() {
    return (
      <div className="form animated flipInX">
        <h2>Update product</h2>
        <form onSubmit={this.submitingUpdatedData}>
          <input placeholder="id" type="text" onChange={this.changeId} />
          <input placeholder="price" type="text" onChange={this.changePrice} />
          <button type="submit">Update</button>
        </form>
      </div>
    );
  }
}

export default UpdateProduct;
