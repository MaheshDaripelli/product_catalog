import "./product.css";

const Product = (props) => {
  const { each } = props;
  return (
    <div className="data-row og-row og-li Experienced Engineering 7.3 Ready to hire Andhra Pradesh Yes">
      <div className="og-li-col og-li-col-1 text-center">1</div>
      <div className="og-li-col og-li-col-2">{each.name}</div>
      <div className="og-li-col og-li-col-3 text-center">
        {each.description}
      </div>
      <div className="og-li-col og-li-col-4 text-center">{each.price}</div>
      <div className="og-li-col og-li-col-5 text-center">{each.discount}</div>
      <div className="og-li-col og-li-col-6 text-center">
        {each.merchant_name}
      </div>
      <div className="og-li-col og-li-col-7 text-center">{each.quantity}</div>
    </div>
  );
};

export default Product;
