import logo from "./logo.svg";
import "./App.css";
import Header from "./components/Header/header";
import ProductView from "./components/ProductView/productview";
import Sidebar from "./components/Sidebar/sidebar";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import CreateProduct from "./components/CreateProduct/createproduct";
import UpdateProduct from "./components/UpdateProduct/updateproduct";
import DeleteProduct from "./components/DeleteProduct/deleteproduct";

function App() {
  return (
    <div>
      <Header />
      <div className="main-div-app">
        <Sidebar />
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={ProductView} />
            <Route exact path="/create_product" component={CreateProduct} />
            <Route exact path="/update_product" component={UpdateProduct} />
            <Route exact path="/delete_product" component={DeleteProduct} />
          </Switch>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
