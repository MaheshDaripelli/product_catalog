import mongoose from "mongoose";

//creating schema for createprofile
const CreateProduct = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  discount: {
    type: Number,
  },
  merchant_name: {
    type: String,
    required: true,
  },
  quantity: {
    type: String,
    required: true,
  },
});

export { CreateProduct };
