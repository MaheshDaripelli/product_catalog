import { CREATE_PRODUCT_DETAILS } from "../utils/mongodb.js";

const update_product = async (request, response) => {
  const data = request.body;
  console.log(data);
  try {
    const result = await CREATE_PRODUCT_DETAILS.updateOne(
      { _id: data.id },
      {
        $set: {
          price: data.price,
        },
      }
    );
    response.send(
      JSON.stringify({
        status_code: 200,
        status_message: "data updated successfully",
      })
    );
    console.log(result);
  } catch (err) {
    //catch block catches the exception
    console.log(err);
  }
};
export { update_product };
