import { CREATE_PRODUCT_DETAILS } from "../utils/mongodb.js";

const get_all_products = (request, response) => {
  CREATE_PRODUCT_DETAILS.find({}, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      response.send(data);
    }
  });
};
export { get_all_products };
