import { CREATE_PRODUCT_DETAILS } from "../utils/mongodb.js";

const create_product = (request, response) => {
  const data = request.body;
  const product = new CREATE_PRODUCT_DETAILS({
    name: data.name,
    description: data.description,
    price: data.price,
    quantity: data.quantity,
    merchant_name: data.merchant_name,
    discount: data.discount,
  });
  product
    .save()
    .then(() =>
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "data added successfully",
        })
      )
    )
    .catch((err) => {
      console.log(err);
    });
};
export { create_product };
