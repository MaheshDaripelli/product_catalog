import { CREATE_PRODUCT_DETAILS } from "../utils/mongodb.js";

const delete_product = (request, response) => {
  const data = request.body;
  CREATE_PRODUCT_DETAILS.findOneAndRemove({ _id: data.id }, (err) => {
    if (err) {
      console.log(err);
    } else {
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "data deleted successfuly",
        })
      );
    }
  });
};
export { delete_product };
