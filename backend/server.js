// importing express from the express module
import mongoose from "mongoose";
import express from "express";
//importign bodyparser from the body-parser module
import bodyParser from "body-parser";
//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//importing cors
import cors from "cors";
//enable the express server to respond to preflight requests
app.use(
  cors({
    origin: "*",
  })
);
import { get_all_products } from "./path_handlers/get_apis.js";
import { create_product } from "./path_handlers/post_apis.js";
import { update_product } from "./path_handlers/put_apis.js";
import { delete_product } from "./path_handlers/delete_apis.js";
import { validation_for_product_data } from "./utils/validation.js";

app.listen(1947, () => {
  console.log("this server is connected to http://localhost:1947");
});

app.get("/products", get_all_products);

app.post("/product/create", validation_for_product_data, create_product);

app.put("/update", update_product);

app.delete("/product/delete", delete_product);
